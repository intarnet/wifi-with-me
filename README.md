Dependencies
============

In order to facilitate dependency management, you can use a pip and
a virtual environment (like virtualenv).

Install packages:

```bash
sudo apt-get install python3-pip python3-virtualenv virtualenv libyaml-dev
```

Retrieve the repository:

```bash
# Create the base directory:
mkdir wwm
# Clone the repo:
git clone https://code.ffdn.org/FFDN/wifi-with-me.git
```

Create and activate the virtualenv with:

```bash
virtualenv -p $(which python3) venv
source venv/bin/activate
```

We use django framework.  Install it from requirements with pip:

```bash
pip install -r wifi-with-me/requirements/base.txt
```

For development, install `dev.txt` instead:

```bash
pip install -r wifi-with-me/requirements/dev.txt
```

Set up configuration
====================

Then create and edit the configuration file
`wifi-with-me/wifiwithme/settings/local.py` according to following
instructions:

Mandatory settings
------------------

You **must** Define some details about your ISP in the ISP variable, eg:

```python
ISP={
    'NAME':'FAIMAISON',
    'TITLE':'Réseau wifi expérimental',
    'SITE':'//www.faimaison.net',
    'EMAIL':'bureau (at) faimaison.net',
    'ZONE':'Nantes et environs',
    'ADDRESS_EXAMPLE': 'rue du calvaire, nantes',
    'URL_CONTACT':'//www.faimaison.net/pages/contact.html',
    'LATITUDE':47.218371,
    'LONGITUDE':-1.553621,
    'ZOOM':13,
    'CNIL':{
        'LINK':'//www.faimaison.net/files/cnil-faimaison-1819684-withwithme.pdf',
        'NUMBER':1819684
    }
}
```

The wifi-with-me website URL (for links included in emails) defaults to
`http://localhost:8000`, so it must be customized:

```python
SITE_URL='https://wifi.faimaison.net'
```

List of domain name(s) under wich the website is accessible:

```python
ALLOWED_HOSTS=['wifi.faimaison.net']
# Or, for several domains:
ALLOWED_HOSTS=['wifi.faimaison.net', 'faimaison.net']
```

Optional settings
-----------------

### Website URL

Optionaly, you can define an url prefix (ex: `/foo/`) so that wifi-with-me is
accessible under *http://example.com/foo/*:

```python
URL_PREFIX='foo/'
```

### Notifications

If you want to receive notifications on each new contrib, customize those:

List of notification recipients:

```python
NOTIFICATION_EMAILS=['admin@example.com', 'admin2@example.com']
```

Notification sender address:

```python
DEFAULT_FROM_EMAIL='notifier@example.tld'
```

### Data expiration

The data gets deleted after one year, if the contributing user does not give
its explicit consent to keep it one more year.

Reminders are sent to the contribution author when expiration date gets
close. By default we send two notifications :

```python
DATA_EXPIRATION_REMINDERS = [
    30,  # 1 month before
    7,   # 1 week before
]
```

You can tweak it to your will or decide to send no reminder (with value `[]`).


Migrate from bottle version (optional)
======================================

If you used the (old) bottle version of wifi-with-me and want to migrate your
data follow this extra step :

```bash
./wifi-with-me/manage.py migrate auth
./wifi-with-me/manage.py migrate contribmap 0001 --fake
```

Run development server
======================

NOTE: For prod environment, replace `./wifi-with-me/manage.py` with
`DJANGO_SETTINGS_MODULE=wifiwithme.settings.prod ./wifi-with-me/manage.py`.

It is required to initialize database first:

```bash
./wifi-with-me/manage.py migrate
```

Create an admin:

```bash
./wifi-with-me/manage.py createsuperuser
```

Then launch service with:

```bash
./wifi-with-me/manage.py runserver
```

You can visit with your browser at <http://127.0.0.1:8000/map/contribute>

Drop the database
=================

If you want to **reset all your data**.

```bash
rm db.sqlite3
```


Run production server
=====================

(Pretty rough instructions. Feel free to submit patches)

1. Deploy it [like any django site](https://docs.djangoproject.com/en/1.11/howto/deployment/)
2. Customize [mandatory and optional settings](#set-up-configuration)
3. Customize `SECRET_KEY` to something really random. Hint:

```bash
python -c "import string,random; uni=string.ascii_letters+string.digits+string.punctuation; print(repr(''.join([random.SystemRandom().choice(uni) for i in range(random.randint(45,50))])))"
```

4. Set *daily* crons for the two commands that take care of data expiration
   handling (run them with `--help` for more information):

```bash
./wifi-with-me/manage.py delete_expired_contribs
./wifi-with-me/manage.py send_expiration_reminders
```

# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.html import format_html

# Register your models here.
from .models import Contrib


# Kinda hackish to do that here
admin.site.site_header = "Administration − Wifi with me"
admin.site.site_title = "Wifi with me"


@admin.register(Contrib)
class ContribAdmin(admin.ModelAdmin):
    search_fields = ["name", "email", "phone"]
    list_display = ("name", "date", "phone", "email", "expired_string")

    readonly_fields = ['date', 'expiration_date']
    fieldsets = [
        [None, {
            'fields': [
                ('name', 'contrib_type'),
                'comment', 'email', 'phone',
                ('date', 'expiration_date'),
            ],
        }],
        ['Localisation', {
            'fields': [
                ('latitude', 'longitude'),
                ('floor', 'floor_total'),
                'orientations', 'roof']
        }],
        ['Partage de connexion', {
            'fields': ['access_type'],
            'classes': ['collapse'],
        }],
        ['Vie privée', {
            'fields': [
                'privacy_name', 'privacy_email', 'privacy_coordinates',
                'privacy_place_details', 'privacy_comment'
            ],
            'classes': ['collapse'],
        }]
    ]

    def expired_string(self, obj):
        if obj.is_expired():
            return format_html('<strong style="color: red; cursor: help;" title="Cette entrée excède la durée de rétention et aurait dû être supprimée automatiquement.">expiré</strong>')
        else:
            return 'non expiré'

    expired_string.short_description = 'Expiration'

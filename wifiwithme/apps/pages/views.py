from django.conf import settings
from django.shortcuts import render


def home(request):
    return render(request, 'pages/home.html', {
        'isp':settings.ISP,
    })
